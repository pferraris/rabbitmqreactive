﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Text;

namespace RabbitMQ.Reactive
{
    public class Producer<T> : IDisposable
    {
        private string exchange;
        private IModel channel;
        private IConnection connection;

        public Producer(string uri, string exchange)
        {
            this.exchange = exchange;
            var factory = new ConnectionFactory() { Uri = new Uri(uri) };
            factory.AutomaticRecoveryEnabled = true;
            connection = factory.CreateConnection();
            channel = connection.CreateModel();
            channel.ExchangeDeclare(exchange, ExchangeType.Fanout, true);
        }

        public void Dispose()
        {
            channel.Dispose();
            if (connection.IsOpen) connection.Close();
            connection.Dispose();
        }

        public void OnNext(T message)
        {
            IBasicProperties basicProperties = channel.CreateBasicProperties();
            basicProperties.Persistent = true;
            basicProperties.ContentType = "application/json";
            basicProperties.Type = typeof(T).ToString();
            channel.BasicPublish(exchange, string.Empty, basicProperties, Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message, Formatting.Indented)));
        }
    }
}
