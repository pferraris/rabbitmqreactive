﻿using System;
using System.Collections.Generic;

namespace RabbitMQ.Reactive
{
    public abstract class Observable<T> : IObservable<T>
    {
        protected List<IObserver<T>> Observers { get; private set; }

        public Observable()
        {
            Observers = new List<IObserver<T>>();
        }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            if (!Observers.Contains(observer))
                Observers.Add(observer);
            return new Unsubscriber(observer, Observers);
        }

        public IDisposable Subscribe(List<IObserver<T>> observers)
        {
            return Subscribe(new Subscriber(observers));
        }

        private class Subscriber : IObserver<T>
        {
            private List<IObserver<T>> observers;

            public Subscriber(List<IObserver<T>> observers)
            {
                this.observers = observers;
            }

            public void OnCompleted()
            {
                observers.ForEach(x => x.OnCompleted());
            }

            public void OnError(Exception error)
            {
                observers.ForEach(x => x.OnError(error));
            }

            public void OnNext(T value)
            {
                observers.ForEach(x => x.OnNext(value));
            }
        }

        private class Unsubscriber : IDisposable
        {
            private IObserver<T> observer;
            private List<IObserver<T>> observers;

            public Unsubscriber(IObserver<T> observer, List<IObserver<T>> observers)
            {
                this.observer = observer;
                this.observers = observers;
            }

            public void Dispose()
            {
                if (observers.Contains(observer))
                    observers.Remove(observer);
            }
        }
    }
}
