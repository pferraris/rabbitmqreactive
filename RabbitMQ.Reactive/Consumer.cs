﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ.Reactive
{
    public class Consumer<T> : Observable<T>, IDisposable
    {
        private string queue;
        private IConnection connection;
        private IModel channel;
        private Task consuming;

        public Consumer(string uri, string queue, string exchange)
        {
            this.queue = queue;
            var factory = new ConnectionFactory { Uri = new Uri(uri) };
            factory.AutomaticRecoveryEnabled = true;
            connection = factory.CreateConnection();
            channel = connection.CreateModel();
            EnsureQueue(exchange);
            consuming = Consume();
        }

        public void Dispose()
        {
            channel.Dispose();
            if (connection.IsOpen) connection.Close();
            connection.Dispose();
        }

        private void EnsureQueue(string exchange)
        {
            channel.ExchangeDeclare(exchange, ExchangeType.Fanout, true);
            channel.QueueDeclare(queue, true, false, false);
            channel.QueueBind(queue, exchange, string.Empty);
        }

        private async Task Consume()
        {
            await Task.Yield();
            var eventingBasicConsumer = new EventingBasicConsumer(channel);
            eventingBasicConsumer.Received += (consumer, args) =>
            {
                try
                {
                    var message = JsonConvert.DeserializeObject<T>(Encoding.UTF8.GetString(args.Body));
                    Observers.ForEach(x => x.OnNext(message));
                    channel.BasicAck(args.DeliveryTag, false);
                }
                catch (Exception)
                {
                    channel.BasicReject(args.DeliveryTag, false);
                }
            };
            channel.BasicConsume(queue, false, eventingBasicConsumer);
        }
    }
}
